function displayAddition(number1, number2){
	let addition = number1 + number2;
	console.log("Displayed sum of " + number1 + " and " + number2);
	console.log(addition);
}

function displaySubtraction(number1, number2){
	let subtraction = number1 - number2;
	console.log("Displayed difference of " + number1 + " and " + number2);
	console.log(subtraction);
}

displayAddition(5,15);
displaySubtraction(20,5);

function displayMultiplication(number1, number2){
	console.log("The product of " + number1 + " and " + number2 + ":");
	return number1 * number2;
}
let product = displayMultiplication(50, 10);
console.log(product);

function displayDivision(number1, number2){
	console.log("The quotient of " + number1 + " and " + number2 + ":");
	return number1 / number2;

}
let quotient = displayDivision(50, 10);
console.log(quotient);

function displayTotalAreaOfCircle(radius){
	console.log("The result of getting the area of a circle with " + radius + " radius:");
	return (Math.PI * (radius**2)).toFixed(2);
}
let circleArea = displayTotalAreaOfCircle(15);
console.log(parseFloat(circleArea));

function displayAverageOf4Numbers(number1, number2, number3, number4){
	console.log("The average of " + number1 + "," + number2 + "," + number3 + " and " + number4 + ":");
	return((number1 + number2 + number3 + number4) / 4 );
}
let averageVar = displayAverageOf4Numbers(20,40,60,80);
console.log(averageVar);

function checkScore(score, totalScore){
	console.log("Is " + score + "/" + totalScore + " a passing score?");
	let resultPercentage = (score / totalScore) * 100;
	let isPassed = resultPercentage >= 75;
	return isPassed;
}
let isPassingScore = checkScore(38,50);
console.log(isPassingScore);